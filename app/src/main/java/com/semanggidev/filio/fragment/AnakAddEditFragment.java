package com.semanggidev.filio.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;

import com.semanggidev.filio.R;
import com.semanggidev.filio.activity.MainActivity;
import com.semanggidev.filio.data.DatabaseDescription.Anak;
import com.semanggidev.filio.tool.DateHandler;

/**
 * Created by anachmad on 5/23/17.
 */

public class AnakAddEditFragment extends Fragment
    implements LoaderManager.LoaderCallbacks<Cursor>{

    // defines callback method implemented by MainActivity
    public interface AnakAddEditFragmentListener {
        // called when contact is saved
        void onAddEditCompleted(Uri contactUri);
    }

    // constant used to identify the Loader
    private static final int ANAK_LOADER = 0;

    private AnakAddEditFragmentListener listener; // MainActivity
    private Uri anakUri; // Uri of selected anak
    private boolean addingNewAnak = true; // adding (true) or editing

    // EditTexts for contact information
    private TextInputLayout namaAnakTextInputLayout;
    private TextInputLayout namaPanggilanAnakTextInputLayout;
    private RadioButton lakilakiRadioButton;
    private RadioButton perempuanRadioButton;
    private EditText tanggalLahirEditText;
    private FloatingActionButton saveAnakFAB;
    private long currentUnixTime;

    private CoordinatorLayout coordinatorLayout; // used with SnackBars

    // set PortofolioAddEditFragmentListener when Fragment attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (AnakAddEditFragmentListener) context;
    }

    // remove PortofolioAddEditFragmentListener when Fragment detached
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // called when Fragment's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // fragment has menu items to display

        // inflate GUI and get references to EditTexts
        View view =
            inflater.inflate(R.layout.fragment_anak_add_edit, container, false);

        namaAnakTextInputLayout = (TextInputLayout) view.findViewById(R.id.namaAnakTextInputLayout);
        namaAnakTextInputLayout.getEditText().addTextChangedListener(namaAnakChangedListener);
        namaPanggilanAnakTextInputLayout = (TextInputLayout) view.findViewById(R.id.namaPanggilanAnakTextInputLayout);
        lakilakiRadioButton = (RadioButton) view.findViewById(R.id.lakilakiRadioButton);
        perempuanRadioButton = (RadioButton) view.findViewById(R.id.perempuanRadioButton);

        currentUnixTime = DateHandler.getCurrentUnixTime();

        // set FloatingActionButton's event listener
        saveAnakFAB = (FloatingActionButton) view.findViewById(
                R.id.saveFloatingActionButton);
        saveAnakFAB.setOnClickListener(saveAnakButtonClicked);
        updateSaveButtonFAB();

        // used to display SnackBars with brief messages
        coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(
                R.id.coordinatorLayout);

        Bundle arguments = getArguments(); // null if creating new contact

        if (arguments != null) {
            addingNewAnak = false;
            anakUri = arguments.getParcelable(MainActivity.ANAK_URI);
        }

        // if editing an existing contact, create Loader to get the contact
        if (anakUri != null)
            getLoaderManager().initLoader(ANAK_LOADER, null, this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_fragment_anak_addedit, menu);
    }

    // handle menu item selections
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        }

        return super.onOptionsItemSelected(item);
    }

    // detects when the text in the judulKegiatanTextInputLayout's EditText changes
    // to hide or show saveButtonFAB
    private final TextWatcher namaAnakChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {}

        // called when the text in nameTextInputLayout changes
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            updateSaveButtonFAB();
        }

        @Override
        public void afterTextChanged(Editable s) { }
    };

    // shows saveButtonFAB only if the judulKegiatan is not empty
    private void updateSaveButtonFAB() {
        String input =
                namaAnakTextInputLayout.getEditText().getText().toString();

        // if there is a judulKegiatan for the portofolio, show the FloatingActionButton
        if (input.trim().length() != 0)
            saveAnakFAB.show();
        else
            saveAnakFAB.hide();
    }


    // responds to event generated when user saves a portofolio
    private final View.OnClickListener saveAnakButtonClicked =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // hide the virtual keyboard
                    ((InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                            getView().getWindowToken(), 0);
                    savePortofolio(); // save portofolio to the database
                }
            };

    // saves portofolio information to the database
    private void savePortofolio() {
        // create ContentValues object containing contact's key-value pairs
        ContentValues contentValues = new ContentValues();

        contentValues.put(Anak.COLUMN_NAMA_ANAK,
                namaAnakTextInputLayout.getEditText().getText().toString());
        contentValues.put(Anak.COLUMN_NAMA_PANGGILAN_ANAK,
                namaPanggilanAnakTextInputLayout.getEditText().getText().toString());
        contentValues.put(Anak.COLUMN_JENIS_KELAMIN,
                namaAnakTextInputLayout.getEditText().getText().toString());

        if (addingNewAnak) {
            // use Activity's ContentResolver to invoke
            // insert on the PortofolioContentProvider
            Uri newAnakUri = getActivity().getContentResolver().insert(
                    Anak.CONTENT_URI, contentValues);

            if (newAnakUri != null) {
                Snackbar.make(coordinatorLayout,
                        R.string.anak_added, Snackbar.LENGTH_LONG).show();
                listener.onAddEditCompleted(newAnakUri);
            }
            else {
                Snackbar.make(coordinatorLayout,
                        R.string.anak_not_added, Snackbar.LENGTH_LONG).show();
            }
        }
        else {
            // use Activity's ContentResolver to invoke
            // insert on the PortofolioContentProvider
            int updatedRows = getActivity().getContentResolver().update(
                    anakUri, contentValues, null, null);

            if (updatedRows > 0) {
                listener.onAddEditCompleted(anakUri);
                Snackbar.make(coordinatorLayout,
                        R.string.anak_updated, Snackbar.LENGTH_LONG).show();
            }
            else {
                Snackbar.make(coordinatorLayout,
                        R.string.anak_not_updated, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        switch (id) {
            case ANAK_LOADER:
                return new CursorLoader(getActivity(),
                        anakUri, // Uri of contact to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
            default:
                return null;
        }
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // if the portofolio exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int namaAnakIndex = data.getColumnIndex(Anak.COLUMN_NAMA_ANAK);
            int namaPanggilanAnakIndex = data.getColumnIndex(Anak.COLUMN_NAMA_PANGGILAN_ANAK);
            int tanggalLahirIndex = data.getColumnIndex(Anak.COLUMN_TANGGAL_LAHIR);
            int jenisKelaminIndex = data.getColumnIndex(Anak.COLUMN_JENIS_KELAMIN);

            // fill EditTexts with the retrieved data
            namaAnakTextInputLayout.getEditText().setText(data.getString(namaAnakIndex));
            namaPanggilanAnakTextInputLayout.getEditText().setText(data.getString(namaPanggilanAnakIndex));
            tanggalLahirEditText.setText(DateHandler.getTanggalBulanTahun(data.getLong(tanggalLahirIndex)));
            if(data.getString(jenisKelaminIndex).equals("P")){
                perempuanRadioButton.setChecked(true);
            } else lakilakiRadioButton.setChecked(true);

            updateSaveButtonFAB();

        }
    }

    // called by LoaderManager when the Loader is being reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }

}
