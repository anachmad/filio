package com.semanggidev.filio.fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.semanggidev.filio.R;
import com.semanggidev.filio.activity.MainActivity;
import com.semanggidev.filio.data.DatabaseDescription.Anak;
import com.semanggidev.filio.dialog.DeletePortofolioDialogFragment;
import com.semanggidev.filio.tool.DateHandler;

/**
 * Created by anachmad on 5/13/17.
 */

public class AnakDetailFragment extends Fragment
    implements LoaderCallbacks<Cursor>,
        DeletePortofolioDialogFragment.DeletePortofolioDialogFragmentListener{

    // callback methods implemented by MainActivity
    public interface AnakDetailFragmentListener {
        void onAnakDeleted(); // called when a anak is deleted

        // pass Uri of contact to edit to the DetailFragmentListener
        void onEditAnak(Uri anakUri);
    }

    private static final int ANAK_LOADER = 0; // identifies the Loader

    private AnakDetailFragmentListener listener; // MainActivity
    private Uri anakUri; // Uri of selected portofolio

    private TextView namaAnakTextView;
    private TextView namaPanggilanAnakTextView;
    private TextView jenisKelaminTextView;
    private TextView tanggalLahirTextView;
    private TextView tempatLahirTextView;

    // set AnakDetailFragmentListener when fragment attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (AnakDetailFragmentListener) context;
    }

    // remove AnakDetailFragmentListener when fragment detached
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // called when AnakDetailFragmentListener's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // this fragment has menu items to display

        // get Bundle of arguments then extract the contact's Uri
        Bundle arguments = getArguments();

        if (arguments != null)
            anakUri = arguments.getParcelable(MainActivity.ANAK_URI);

        // inflate DetailFragment's layout
        View view =
                inflater.inflate(R.layout.fragment_anak_detail, container, false);

        // get the EditTexts
        namaAnakTextView = (TextView) view.findViewById(R.id.namaAnakTextView);
        namaPanggilanAnakTextView = (TextView) view.findViewById(R.id.namaPanggilanTextView);
        jenisKelaminTextView = (TextView) view.findViewById(R.id.jenisKelamintextView);
        tanggalLahirTextView = (TextView) view.findViewById(R.id.tanggalLahirTextView);

        // load anak
        getLoaderManager().initLoader(ANAK_LOADER, null, this);
        return view;
    }

    // display this fragment's menu items
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_anak_detail, menu);
    }

    // handle menu item selections
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                listener.onEditAnak(anakUri); // pass Uri to listener
                return true;
            case R.id.action_delete:
                deleteAnak();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // delete a portofolio
    private void deleteAnak() {
        // use FragmentManager to display the confirmDelete DialogFragment
//        confirmDelete.show(getFragmentManager(), "confirm delete");
        FragmentManager fm = getFragmentManager();
        DeletePortofolioDialogFragment dialog = new DeletePortofolioDialogFragment();
        dialog.setTargetFragment(AnakDetailFragment.this,300);
        dialog.show(fm,"delete_dialog");
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        CursorLoader cursorLoader;

        switch (id) {
            case ANAK_LOADER:
                cursorLoader = new CursorLoader(getActivity(),
                        anakUri, // Uri of portofolio to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
                break;
            default:
                cursorLoader = null;
                break;
        }

        return cursorLoader;
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // if the portofolio exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int namaAnakIndex = data.getColumnIndex(Anak.COLUMN_NAMA_ANAK);
            int namaPanggilanAnakIndex = data.getColumnIndex(Anak.COLUMN_NAMA_PANGGILAN_ANAK);
            int jenisKelaminAnakIndex = data.getColumnIndex(Anak.COLUMN_JENIS_KELAMIN);
            int tanggalLahirAnakIndex = data.getColumnIndex(Anak.COLUMN_TANGGAL_LAHIR);

            // fill TextViews with the retrieved data
            tanggalLahirTextView.setText(DateHandler.getTanggalBulanTahun(data.getLong(tanggalLahirAnakIndex)));
            namaAnakTextView.setText(data.getString(namaAnakIndex));
            namaPanggilanAnakTextView.setText(data.getString(namaPanggilanAnakIndex));
            jenisKelaminTextView.setText(data.getString(jenisKelaminAnakIndex));
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onDialogPositiveClick() {
        getActivity().getContentResolver().delete(
                anakUri, null, null);
        listener.onAnakDeleted();
    }

}
