package com.semanggidev.filio.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.semanggidev.filio.R;
import com.semanggidev.filio.activity.MainActivity;
import com.semanggidev.filio.data.DatabaseDescription.Portofolio;
import com.semanggidev.filio.dialog.AspekPerkembanganDialogFragment;
import com.semanggidev.filio.dialog.DatePickerDialogFragment;
import com.semanggidev.filio.dialog.EkspresiAnakDialogFragment;
import com.semanggidev.filio.dialog.FitrahDialogFragment;
import com.semanggidev.filio.tool.DateHandler;
import com.semanggidev.filio.tool.ImageHandler;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by anachmad on 5/23/17.
 */

public class PortofolioAddEditFragment extends Fragment
    implements LoaderManager.LoaderCallbacks<Cursor>,
        FitrahDialogFragment.FitrahDialogListener,
        AspekPerkembanganDialogFragment.AspekPerkembanganDialogListener,
        EkspresiAnakDialogFragment.EkspresiAnakDialogFragmentListener,
        DatePickerDialogFragment.DatePickerDialogFragmentListener{

    @Override
    public void onFitrahDialogPositiveClick(String selectedItemsString) {
        fitrahTextInputLayout.getEditText().setText(selectedItemsString);
    }

    @Override
    public void onAspekPerkembanganDialogPositiveClick(String selectedItemsString) {
        aspekPerkembanganTextInputLayout.getEditText().setText(selectedItemsString);
    }

    @Override
    public void onImageSelected(int id) {
        switch (id){
            case R.id.imageViewSenang:
                ekspresiAnakImageView.setImageResource(R.mipmap.ic_happy);
                ekspresiAnakImageView.setTag("ic_happy");
                break;
            case R.id.imageViewSedih:
                ekspresiAnakImageView.setImageResource(R.mipmap.ic_sad);
                ekspresiAnakImageView.setTag("ic_sad");
                break;
            case R.id.imageViewMarah:
                ekspresiAnakImageView.setImageResource(R.mipmap.ic_angry);
                ekspresiAnakImageView.setTag("ic_angry");
                break;
            case R.id.imageViewTakut:
                ekspresiAnakImageView.setImageResource(R.mipmap.ic_afraid);
                ekspresiAnakImageView.setTag("ic_afraid");
                break;
            case R.id.imageViewJijik:
                ekspresiAnakImageView.setImageResource(R.mipmap.ic_disgust);
                ekspresiAnakImageView.setTag("ic_disgust");
                break;
        }
    }

    @Override
    public void onDatePickerDialogDateSet(int year, int month, int dayOfMonth) {
        final Calendar calendar =  Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        chosenUnixTime = DateHandler.toUnixTime(calendar.getTime());
        setTimeLabel(chosenUnixTime);

    }

    // defines callback method implemented by MainActivity
    public interface PortofolioAddEditFragmentListener {
        // called when contact is saved
        void onAddEditCompleted(Uri contactUri);
    }

    // constant used to identify the Loader
    private static final int PORTOFOLIO_LOADER = 0;

    private PortofolioAddEditFragmentListener listener; // MainActivity
    private Uri portofolioUri; // Uri of selected contact
    private boolean addingNewPortofolio = true; // adding (true) or editing

    // EditTexts for contact information
    private TextView tanggalTextView;
    private TextView hariTextView;
    private TextView bulanTahunTextView;
    private TextView waktuTextView;
    private TextInputLayout judulKegiatanTextInputLayout;
    private TextInputLayout tempatKegiatanTextInputLayout;
    private TextInputLayout namaAnakTextInputLayout;
    private TextInputLayout deskripsiKegiatanTextInputLayout;
    private TextInputLayout tujuanKegiatanTextInputLayout;
    private TextInputLayout uraianEkspresiAnakTextInputLayout;
    private TextInputLayout hikmahKegiatanTextInputLayout;
    private TextInputLayout tindakLanjutTextInputLayout;
    private TextInputLayout doaTextInputLayout;
    private TextInputLayout fitrahTextInputLayout;
    private TextInputLayout aspekPerkembanganTextInputLayout;
    private TextInputLayout fasilitatorTextInputLayout;
    private FloatingActionButton savePortofolioFAB;
    private ImageView ekspresiAnakImageView;
    private ImageView gambarKegiatanImageView;
    private Uri photoUri;
    private long chosenUnixTime;
    private ArrayList<Image> images = new ArrayList<>();

    private CoordinatorLayout coordinatorLayout; // used with SnackBars

    private DateHandler dateHandler;

    // set PortofolioAddEditFragmentListener when Fragment attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (PortofolioAddEditFragmentListener) context;
    }

    // remove PortofolioAddEditFragmentListener when Fragment detached
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // called when Fragment's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // fragment has menu items to display

        // inflate GUI and get references to EditTexts
        View view =
                inflater.inflate(R.layout.fragment_portofolio_add_edit_new, container, false);

        tanggalTextView =
                (TextView) view.findViewById(R.id.tanggalTextView);
        hariTextView =
                (TextView) view.findViewById(R.id.hariTextView);
        bulanTahunTextView =
                (TextView) view.findViewById(R.id.bulanTahunTextView);
        waktuTextView =
                (TextView) view.findViewById(R.id.waktuTextView);
        judulKegiatanTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.judulKegiatanTextInputLayout);
        judulKegiatanTextInputLayout.getEditText().addTextChangedListener(
                judulKegiatanChangedListener);
        tempatKegiatanTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.tempatKegiatanTextInputLayout);
        namaAnakTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.namaAnakTextInputLayout);
        deskripsiKegiatanTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.deskripsiKegiatanTextInputLayout);
        tujuanKegiatanTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.tujuanKegiatanTextInputLayout);
        uraianEkspresiAnakTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.uraianEkspresiAnakTextInputLayout);
        fitrahTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.fitrahTextInputLayout);
        aspekPerkembanganTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.aspekPerkembanganTextInputLayout);
        fasilitatorTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.fasilitatorTextInputLayout);
        hikmahKegiatanTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.hikmahKegiatanTextInputLayout);
        tindakLanjutTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.tindakLanjutTextInputLayout);
        doaTextInputLayout =
                (TextInputLayout) view.findViewById(R.id.doaTextInputLayout);
        ekspresiAnakImageView =
                (ImageView) view.findViewById(R.id.ekspresiAnakImageView);
        gambarKegiatanImageView =
                (ImageView) view.findViewById(R.id.gambarKegiatanImageView);

        gambarKegiatanImageView.setVisibility(View.GONE);

        chosenUnixTime = DateHandler.getCurrentUnixTime();
        setTimeLabel(chosenUnixTime);

        ekspresiAnakImageView.setImageResource(R.mipmap.ic_happy);
        ekspresiAnakImageView.setTag("ic_happy");

        // set FloatingActionButton's event listener
        savePortofolioFAB = (FloatingActionButton) view.findViewById(
                R.id.saveFloatingActionButton);
        savePortofolioFAB.setOnClickListener(savePortofolioButtonClicked);
        updateSaveButtonFAB();

        // used to display SnackBars with brief messages
        coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(
                R.id.coordinatorLayout);

        Bundle arguments = getArguments(); // null if creating new contact

        if (arguments != null) {
            addingNewPortofolio = false;
            portofolioUri = arguments.getParcelable(MainActivity.PORTOFOLIO_URI);
        }

        // if editing an existing contact, create Loader to get the contact
        if (portofolioUri != null)
            getLoaderManager().initLoader(PORTOFOLIO_LOADER, null, this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tanggalTextView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        ekspresiAnakImageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showEkspresiAnakDialog();
            }
        });

        fitrahTextInputLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener(){

            public void onFocusChange(View v, boolean hasFocus){
                if(hasFocus){
                    showFitrahDialog(fitrahTextInputLayout.getEditText().getText().toString());
                }
            }
        });

        fitrahTextInputLayout.getEditText().setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                showFitrahDialog(fitrahTextInputLayout.getEditText().getText().toString());
            }
        });

        aspekPerkembanganTextInputLayout.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAspekPerkembanganDialog(aspekPerkembanganTextInputLayout.getEditText().toString());
            }
        });

        aspekPerkembanganTextInputLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    showAspekPerkembanganDialog(aspekPerkembanganTextInputLayout.getEditText().toString());
                }
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_fragment_portofolio_addedit, menu);
    }

    // handle menu item selections
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_photo:
                showImagePicker();
//        PortofolioAddEditFragmentPermissionsDispatcher.onPickPhotoWithCheck(this,getView());
//                ImagePicker.create(this).single().start(ImagePicker.MODE_SINGLE);
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showImagePicker() {
        ImagePicker imagePicker = ImagePicker.create(this)
                .folderMode(true); // set folder mode (false by default)

        imagePicker.multi(); // multi mode (default mode)
        imagePicker.origin(images); // original selected images, used in multi mode
        imagePicker.limit(10) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()) // can be full path
                .start(); // start image picker activity with request code
    }

    // detects when the text in the judulKegiatanTextInputLayout's EditText changes
    // to hide or show saveButtonFAB
    private final TextWatcher judulKegiatanChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {}

        // called when the text in nameTextInputLayout changes
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            updateSaveButtonFAB();
        }

        @Override
        public void afterTextChanged(Editable s) { }
    };

    // shows saveButtonFAB only if the judulKegiatan is not empty
    private void updateSaveButtonFAB() {
        String input =
                judulKegiatanTextInputLayout.getEditText().getText().toString();

        // if there is a judulKegiatan for the portofolio, show the FloatingActionButton
        if (input.trim().length() != 0)
            savePortofolioFAB.show();
        else
            savePortofolioFAB.hide();
    }


    // responds to event generated when user saves a portofolio
    private final View.OnClickListener savePortofolioButtonClicked =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // hide the virtual keyboard
                    ((InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                            getView().getWindowToken(), 0);
                    savePortofolio(); // save portofolio to the database
                }
            };

    // saves portofolio information to the database
    private void savePortofolio() {
        // create ContentValues object containing contact's key-value pairs
        ContentValues contentValues = new ContentValues();

        contentValues.put(Portofolio.COLUMN_TANGGAL_KEGIATAN,
                chosenUnixTime);
        contentValues.put(Portofolio.COLUMN_JUDUL_KEGIATAN,
                judulKegiatanTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_NAMA_ANAK,
                namaAnakTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_TEMPAT_KEGIATAN,
                tempatKegiatanTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_FITRAH,
                fitrahTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_ASPEK_PERKEMBANGAN,
                aspekPerkembanganTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_FASILITATOR,
                fasilitatorTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_DESKRIPSI_KEGIATAN,
                deskripsiKegiatanTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_TUJUAN_KEGIATAN,
                tujuanKegiatanTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_EKSPRESI_ANAK,
                ekspresiAnakImageView.getTag().toString());
        contentValues.put(Portofolio.COLUMN_URAIAN_EKSPRESI_ANAK,
                uraianEkspresiAnakTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_HIKMAH_KEGIATAN,
                hikmahKegiatanTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_FOLLOW_UP,
                tindakLanjutTextInputLayout.getEditText().getText().toString());
        contentValues.put(Portofolio.COLUMN_DOA,
                doaTextInputLayout.getEditText().getText().toString());
//        if(photoUri != null) {
//            contentValues.put(Portofolio.COLUMN_FOTO, photoUri.toString());
//        }
        contentValues.put(Portofolio.COLUMN_FOTO, ImageHandler.createImagesPath(images));

        if (addingNewPortofolio) {
            // use Activity's ContentResolver to invoke
            // insert on the PortofolioContentProvider
            Uri newPortofolioUri = getActivity().getContentResolver().insert(
                    Portofolio.CONTENT_URI, contentValues);

            if (newPortofolioUri != null) {
                Snackbar.make(coordinatorLayout,
                        R.string.portofolio_added, Snackbar.LENGTH_LONG).show();
                listener.onAddEditCompleted(newPortofolioUri);
            }
            else {
                Snackbar.make(coordinatorLayout,
                        R.string.portofolio_not_added, Snackbar.LENGTH_LONG).show();
            }
        }
        else {
            // use Activity's ContentResolver to invoke
            // insert on the PortofolioContentProvider
            int updatedRows = getActivity().getContentResolver().update(
                    portofolioUri, contentValues, null, null);

            if (updatedRows > 0) {
                listener.onAddEditCompleted(portofolioUri);
                Snackbar.make(coordinatorLayout,
                        R.string.portofolio_updated, Snackbar.LENGTH_LONG).show();
            }
            else {
                Snackbar.make(coordinatorLayout,
                        R.string.portofolio_not_updated, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        switch (id) {
            case PORTOFOLIO_LOADER:
                return new CursorLoader(getActivity(),
                        portofolioUri, // Uri of contact to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
            default:
                return null;
        }
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // if the portofolio exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int judulKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_JUDUL_KEGIATAN);
            int tempatKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_TEMPAT_KEGIATAN);
            int tanggalKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_TANGGAL_KEGIATAN);
            int namaAnakIndex = data.getColumnIndex(Portofolio.COLUMN_NAMA_ANAK);
            int fitrahIndex = data.getColumnIndex(Portofolio.COLUMN_FITRAH);
            int aspekPerkembanganIndex = data.getColumnIndex(Portofolio.COLUMN_ASPEK_PERKEMBANGAN);
            int fasilitatorIndex = data.getColumnIndex(Portofolio.COLUMN_FASILITATOR);
            int deskripsiKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_DESKRIPSI_KEGIATAN);
            int tujuanKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_TUJUAN_KEGIATAN);
            int ekspresiAnakIndex = data.getColumnIndex(Portofolio.COLUMN_EKSPRESI_ANAK);
            int uraianEkspresiAnakIndex = data.getColumnIndex(Portofolio.COLUMN_URAIAN_EKSPRESI_ANAK);
            int hikmahKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_HIKMAH_KEGIATAN);
            int tindakLanjutIndex = data.getColumnIndex(Portofolio.COLUMN_FOLLOW_UP);
            int doaIndex = data.getColumnIndex(Portofolio.COLUMN_DOA);

            int fotoIndex = data.getColumnIndex(Portofolio.COLUMN_FOTO);

            // fill EditTexts with the retrieved data

            chosenUnixTime = data.getLong(tanggalKegiatanIndex);
            setTimeLabel(chosenUnixTime);
//            tanggalTextView.setText(DateHandler.getTanggal(data.getLong(tanggalKegiatanIndex)));
//            hariTextView.setText(DateHandler.getHari(data.getLong(tanggalKegiatanIndex)));
//            bulanTahunTextView.setText(DateHandler.getBulan(data.getLong(tanggalKegiatanIndex)) + " " + DateHandler.getTahun(data.getLong(tanggalKegiatanIndex)));
//            waktuTextView.setText(DateHandler.getWaktu(data.getLong(tanggalKegiatanIndex)));

            judulKegiatanTextInputLayout.getEditText().setText(
                    data.getString(judulKegiatanIndex));
            tempatKegiatanTextInputLayout.getEditText().setText(
                    data.getString(tempatKegiatanIndex));
            namaAnakTextInputLayout.getEditText().setText(
                    data.getString(namaAnakIndex));
            deskripsiKegiatanTextInputLayout.getEditText().setText(
                    data.getString(deskripsiKegiatanIndex));
            tujuanKegiatanTextInputLayout.getEditText().setText(
                    data.getString(tujuanKegiatanIndex));
            fitrahTextInputLayout.getEditText().setText(
                    data.getString(fitrahIndex));
            aspekPerkembanganTextInputLayout.getEditText().setText(
                    data.getString(aspekPerkembanganIndex));
            fasilitatorTextInputLayout.getEditText().setText(
                    data.getString(fasilitatorIndex));
            uraianEkspresiAnakTextInputLayout.getEditText().setText(
                    data.getString(uraianEkspresiAnakIndex));
            hikmahKegiatanTextInputLayout.getEditText().setText(
                    data.getString(hikmahKegiatanIndex));
            tindakLanjutTextInputLayout.getEditText().setText(
                    data.getString(tindakLanjutIndex));
            doaTextInputLayout.getEditText().setText(
                    data.getString(doaIndex));

            String imageName = data.getString(ekspresiAnakIndex) == null ? "ic_happy" : data.getString(ekspresiAnakIndex);
            ekspresiAnakImageView.setImageResource(getContext().getResources().getIdentifier(imageName,"mipmap",getActivity().getPackageName()));
            ekspresiAnakImageView.setTag(imageName);


            if(data.getString(fotoIndex) != null) {
                images = ImageHandler.createImagesFromPath(data.getString(fotoIndex));

                RequestOptions option = new RequestOptions();
                option.centerCrop();

                Glide.with(this)
                        .load(images.get(0).getPath())
                        .apply(option)
                        .into(gambarKegiatanImageView);
                gambarKegiatanImageView.setVisibility(View.VISIBLE);
            }

            updateSaveButtonFAB();

        }
    }

    // called by LoaderManager when the Loader is being reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }

    private void showFitrahDialog(String selectedItems){
        FragmentManager fm = getFragmentManager();
        FitrahDialogFragment dialog = FitrahDialogFragment.newInstance(selectedItems);
        dialog.setTargetFragment(PortofolioAddEditFragment.this,300);
        dialog.show(fm,"fitrahDialog");
    }

    private void showAspekPerkembanganDialog(String selectedItems){
        FragmentManager fm = getFragmentManager();
        AspekPerkembanganDialogFragment dialog = AspekPerkembanganDialogFragment.newInstance(selectedItems);
        dialog.setTargetFragment(PortofolioAddEditFragment.this,300);
        dialog.show(fm,"aspekPerkembanganDialog");
    }

    private void showEkspresiAnakDialog(){
        FragmentManager fm = getFragmentManager();
        EkspresiAnakDialogFragment dialog = new EkspresiAnakDialogFragment();
        dialog.setTargetFragment(PortofolioAddEditFragment.this,300);
        dialog.show(fm,"ekspresiAnakDialog");
    }

    private void showDatePickerDialog(){
        FragmentManager fm = getFragmentManager();
        DatePickerDialogFragment dialog = new DatePickerDialogFragment();
        dialog.setTargetFragment(PortofolioAddEditFragment.this, 300);
        dialog.show(fm,"datePickerDialog");
    }

    private void setTimeLabel(long unixTime){
        tanggalTextView.setText(DateHandler.getTanggal(unixTime));
        hariTextView.setText(DateHandler.getHari(unixTime));
        bulanTahunTextView.setText(DateHandler.getBulan(unixTime) + " " + DateHandler.getTahun(unixTime));
        waktuTextView.setText(DateHandler.getWaktu(unixTime));
    }

    /*// PICK_PHOTO_CODE is a constant integer
    public final static int PICK_PHOTO_CODE = 1046;

    // Trigger gallery selection for a photo

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void onPickPhoto(View view) {
        // Create intent for picking a photo from the gallery
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Bring up gallery to select a photo
            startActivityForResult(intent, PICK_PHOTO_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            photoUri = data.getData();
            // Do something with the photo based on Uri
            Bitmap selectedImage = null;
            try {
                selectedImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoUri);
                gambarKegiatanImageView.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Load the selected image into a preview
            gambarKegiatanImageView.setImageBitmap(selectedImage);
        }
    }*/

    private static final int RC_CAMERA = 3000;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_CAMERA) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImage();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);
            showImages(images);
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void showImages(ArrayList<Image> images) {
        RequestOptions option = new RequestOptions();
        option.centerCrop();

        Glide.with(this)
                .load(images.get(0).getPath())
                .apply(option)
                .into(gambarKegiatanImageView);
    }

/*    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PortofolioAddEditFragmentPermissionsDispatcher.onRequestPermissionsResult(this,requestCode,grantResults);
    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
    void showRationaleForCamera(PermissionRequest request) {
        // NOTE: Show a rationale to explain why the permission is needed, e.g. with a dialog.
        // Call proceed() or cancel() on the provided PermissionRequest to continue or abort
        showRationaleDialog(R.string.permission_gallery_rationale, request);
    }

    private void showRationaleDialog(@StringRes int messageResId, final PermissionRequest request) {
        new AlertDialog.Builder(this.getActivity())
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .setCancelable(false)
                .setMessage(messageResId)
                .show();
    }*/

}
