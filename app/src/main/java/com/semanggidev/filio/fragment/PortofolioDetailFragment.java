package com.semanggidev.filio.fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.model.Image;
import com.semanggidev.filio.R;
import com.semanggidev.filio.activity.MainActivity;
import com.semanggidev.filio.data.DatabaseDescription.Portofolio;
import com.semanggidev.filio.dialog.DeletePortofolioDialogFragment;
import com.semanggidev.filio.tool.DateHandler;
import com.semanggidev.filio.tool.ImageHandler;

import java.util.ArrayList;

/**
 * Created by anachmad on 5/13/17.
 */

public class PortofolioDetailFragment extends Fragment
    implements LoaderCallbacks<Cursor>,
        DeletePortofolioDialogFragment.DeletePortofolioDialogFragmentListener{

    // callback methods implemented by MainActivity
    public interface PortofolioDetailFragmentListener {
        // called when a contact is deleted
        void onPortofolioDeleted();

        // pass Uri of contact to edit to the DetailFragmentListener
        void onEditPortofolio(Uri portofolioUri);

        // called upon click on the portofolio image, pass the path of the images
        void onClickImage(ArrayList<String> imagesPathList);
    }

    private static final int PORTOFOLIO_LOADER = 0; // identifies the Loader

    private PortofolioDetailFragmentListener listener; // MainActivity
    private Uri portofolioUri; // Uri of selected portofolio

    private TextView tanggalTextView;
    private TextView hariTextView;
    private TextView bulanTahunTextView;
    private TextView waktuTextView;
    private TextView judulKegiatanTextView; // displays judul kegiatan
    private TextView tempatKegiatanTextView; // displays tempat kegiatan
    private TextView fitrahTextView; // displays fitrah
    private TextView aspekPerkembanganTextView; // displays aspek perkembangan
    private TextView fasilitatorTextView; // displays fasilitator
    private TextView namaAnakTextView;
    private TextView deskripsiKegiatanTextView;
    private TextView tujuanKegiatanTextView;
    private TextView uraianEkspresiAnakTextView;
    private TextView hikmahKegiatanTextView;
    private TextView tindakLanjutTextView;
    private TextView doaTextView;
    private ImageView gambarKegiatanImageView;
    private ImageView gambarEkspresiAnakImageView;
    private ArrayList<Image> images = new ArrayList<>();

    // set PortofolioDetailFragmentListener when fragment attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (PortofolioDetailFragmentListener) context;
    }

    // remove PortofolioDetailFragmentListener when fragment detached
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // called when PortofolioDetailFragmentListener's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // this fragment has menu items to display

        // get Bundle of arguments then extract the contact's Uri
        Bundle arguments = getArguments();

        if (arguments != null)
            portofolioUri = arguments.getParcelable(MainActivity.PORTOFOLIO_URI);

        // inflate DetailFragment's layout
        View view =
                inflater.inflate(R.layout.fragment_portofolio_detail_new, container, false);

        // get the EditTexts
        judulKegiatanTextView = (TextView) view.findViewById(R.id.judulKegiatanTextView);
        tanggalTextView = (TextView) view.findViewById(R.id.tanggalTextView);
        hariTextView = (TextView) view.findViewById(R.id.hariTextView);
        bulanTahunTextView = (TextView) view.findViewById(R.id.bulanTahunTextView);
        waktuTextView = (TextView) view.findViewById(R.id.waktuTextView);
        tempatKegiatanTextView = (TextView) view.findViewById(R.id.tempatKegiatanTextView);
        fitrahTextView = (TextView) view.findViewById(R.id.fitrahTextView);
        aspekPerkembanganTextView = (TextView) view.findViewById(R.id.aspekPerkembanganTextView);
        fasilitatorTextView = (TextView) view.findViewById(R.id.fasilitatorTextView);
        deskripsiKegiatanTextView = (TextView) view.findViewById(R.id.deskripsiKegiatanTextView);
        tujuanKegiatanTextView = (TextView) view.findViewById(R.id.tujuanKegiatanTextView);
        uraianEkspresiAnakTextView = (TextView) view.findViewById(R.id.ekspresiAnakTextView);
        hikmahKegiatanTextView = (TextView) view.findViewById(R.id.hikmahTextView);
        tindakLanjutTextView = (TextView) view.findViewById(R.id.followUpTextView);
        doaTextView = (TextView) view.findViewById(R.id.doaTextView);
        gambarKegiatanImageView = (ImageView) view.findViewById(R.id.fotoKegiatanImageView);
        namaAnakTextView = (TextView) view.findViewById(R.id.namaAnakTextView);
        gambarEkspresiAnakImageView = (ImageView) view.findViewById(R.id.ekspresiAnakImageView);
        gambarKegiatanImageView.setVisibility(View.GONE);
        gambarEkspresiAnakImageView.setVisibility(View.VISIBLE);

        // load the contact
        getLoaderManager().initLoader(PORTOFOLIO_LOADER, null, this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gambarKegiatanImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickImage(ImageHandler.createImagesPathList(images));
            }
        });

    }

    // display this fragment's menu items
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_portofolio_detail, menu);
    }

    // handle menu item selections
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                listener.onEditPortofolio(portofolioUri); // pass Uri to listener
                return true;
            case R.id.action_delete:
                deletePortofolio();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // delete a portofolio
    private void deletePortofolio() {
        // use FragmentManager to display the confirmDelete DialogFragment
//        confirmDelete.show(getFragmentManager(), "confirm delete");
        FragmentManager fm = getFragmentManager();
        DeletePortofolioDialogFragment dialog = new DeletePortofolioDialogFragment();
        dialog.setTargetFragment(PortofolioDetailFragment.this,300);
        dialog.show(fm,"delete_dialog");
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        CursorLoader cursorLoader;

        switch (id) {
            case PORTOFOLIO_LOADER:
                cursorLoader = new CursorLoader(getActivity(),
                        portofolioUri, // Uri of portofolio to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
                break;
            default:
                cursorLoader = null;
                break;
        }

        return cursorLoader;
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // if the portofolio exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int judulKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_JUDUL_KEGIATAN);
            int tempatKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_TEMPAT_KEGIATAN);
            int tanggalKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_TANGGAL_KEGIATAN);
            int namaAnakIndex = data.getColumnIndex(Portofolio.COLUMN_NAMA_ANAK);
            int fitrahIndex = data.getColumnIndex(Portofolio.COLUMN_FITRAH);
            int aspekPerkembanganIndex = data.getColumnIndex(Portofolio.COLUMN_ASPEK_PERKEMBANGAN);
            int fasilitatorIndex = data.getColumnIndex(Portofolio.COLUMN_FASILITATOR);
            int deskripsiKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_DESKRIPSI_KEGIATAN);
            int tujuanKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_TUJUAN_KEGIATAN);
            int ekspresiAnakIndex = data.getColumnIndex(Portofolio.COLUMN_EKSPRESI_ANAK);
            int uraianEkspresiAnakIndex = data.getColumnIndex(Portofolio.COLUMN_URAIAN_EKSPRESI_ANAK);
            int hikmahKegiatanIndex = data.getColumnIndex(Portofolio.COLUMN_HIKMAH_KEGIATAN);
            int tindakLanjutIndex = data.getColumnIndex(Portofolio.COLUMN_FOLLOW_UP);
            int doaIndex = data.getColumnIndex(Portofolio.COLUMN_DOA);
            int fotoIndex = data.getColumnIndex(Portofolio.COLUMN_FOTO);

            // fill TextViews with the retrieved data

            tanggalTextView.setText(DateHandler.getTanggal(data.getLong(tanggalKegiatanIndex)));
            hariTextView.setText(DateHandler.getHari(data.getLong(tanggalKegiatanIndex)));
            bulanTahunTextView.setText(DateHandler.getBulan(data.getLong(tanggalKegiatanIndex))+" "+DateHandler.getTahun(data.getLong(tanggalKegiatanIndex)));
            waktuTextView.setText(DateHandler.getWaktu(data.getLong(tanggalKegiatanIndex)));
            namaAnakTextView.setText(data.getString(namaAnakIndex));
            judulKegiatanTextView.setText(data.getString(judulKegiatanIndex));
            tempatKegiatanTextView.setText(data.getString(tempatKegiatanIndex));
            fitrahTextView.setText(data.getString(fitrahIndex));
            aspekPerkembanganTextView.setText(data.getString(aspekPerkembanganIndex));
            fasilitatorTextView.setText(data.getString(fasilitatorIndex));
            deskripsiKegiatanTextView.setText(data.getString(deskripsiKegiatanIndex));
            tujuanKegiatanTextView.setText(data.getString(tujuanKegiatanIndex));
            uraianEkspresiAnakTextView.setText(data.getString(uraianEkspresiAnakIndex));
            hikmahKegiatanTextView.setText(data.getString(hikmahKegiatanIndex));
            tindakLanjutTextView.setText(data.getString(tindakLanjutIndex));
            doaTextView.setText(data.getString(doaIndex));

            String imageName = data.getString(ekspresiAnakIndex) == null ? "ic_happy" : data.getString(ekspresiAnakIndex);
            gambarEkspresiAnakImageView.setImageResource(getContext().getResources().getIdentifier(imageName,"mipmap",getActivity().getPackageName()));

            if(data.getString(fotoIndex) != null) {
                images = ImageHandler.createImagesFromPath(data.getString(fotoIndex));

                RequestOptions option = new RequestOptions();
                option.centerCrop();

                Glide.with(this)
                        .load(images.get(0).getPath())
                        .apply(option)
                        .into(gambarKegiatanImageView);
                gambarKegiatanImageView.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onDialogPositiveClick() {
        getActivity().getContentResolver().delete(
                portofolioUri, null, null);
        listener.onPortofolioDeleted();
    }

}
