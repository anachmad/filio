package com.semanggidev.filio.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.semanggidev.filio.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TentangFragment extends Fragment {

    private ImageView imageViewLogoFilio;

    public TentangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tentang, container, false);

        imageViewLogoFilio = view.findViewById(R.id.image_filio);

        Glide.with(this)
                .load(R.drawable.icon_filio_big)
                .into(imageViewLogoFilio);

        return view;
    }

}
