package com.semanggidev.filio.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.semanggidev.filio.R;
import com.semanggidev.filio.adapter.ImagesAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PortofolioImageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class PortofolioImageFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private ImagesAdapter imagesAdapter;
    private ArrayList<String> imagesPathList;

    public PortofolioImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_portfolio_image, container, false);

        RecyclerView recyclerViewImages = view.findViewById(R.id.recyclerViewImages);

        GridLayoutManager layout = new GridLayoutManager(this.getActivity(),2);

        recyclerViewImages.setLayoutManager(layout);

        Bundle arguments = getArguments();
        if(arguments != null){
            imagesPathList = arguments.getStringArrayList("images_path_list");
        }

        imagesAdapter = new ImagesAdapter(this.getActivity(), imagesPathList,
                new ImagesAdapter.ImageClickListener() {
                    @Override
                    public void onClick(ArrayList<String> imagesPathList, int position) {

                    }
                });

        recyclerViewImages.setAdapter(imagesAdapter);

        recyclerViewImages.setHasFixedSize(true);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
