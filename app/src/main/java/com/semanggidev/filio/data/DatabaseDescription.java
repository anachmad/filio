package com.semanggidev.filio.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by anachmad on 5/21/17.
 */

public class DatabaseDescription {
    // ContentProvider's name: typically the package name
    public static final String AUTHORITY =
            "com.semanggidev.filio.data";

    // base URI used to interact with the ContentProvider
    private static final Uri BASE_CONTENT_URI =
            Uri.parse("content://" + AUTHORITY);

    // nested class defines contents of the portofolios table
    public static final class Portofolio implements BaseColumns {
        public static final String TABLE_NAME = "portofolios"; // table's name

        // Uri for the portofolios table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        // column names for portofolios table's columns
        public static final String COLUMN_JUDUL_KEGIATAN = "judul_kegiatan";
        public static final String COLUMN_TANGGAL_KEGIATAN = "tanggal_kegiatan";
        public static final String COLUMN_WAKTU_KEGIATAN = "waktu_kegiatan";
        public static final String COLUMN_TEMPAT_KEGIATAN = "tempat_kegiatan";
        public static final String COLUMN_NAMA_ANAK = "nama_anak";
        public static final String COLUMN_USIA_ANAK = "usia_anak";
        public static final String COLUMN_FITRAH = "fitrah";
        public static final String COLUMN_ASPEK_PERKEMBANGAN = "aspek_perkembangan";
        public static final String COLUMN_FASILITATOR = "fasilitator";
        public static final String COLUMN_DESKRIPSI_KEGIATAN = "deskripsi_kegiatan";
        public static final String COLUMN_TUJUAN_KEGIATAN = "tujuan_kegiatan";
        public static final String COLUMN_EKSPRESI_ANAK = "ekspresi_anak";
        public static final String COLUMN_URAIAN_EKSPRESI_ANAK = "uraian_ekspresi_anak";
        public static final String COLUMN_HIKMAH_KEGIATAN = "hikmah_kegiatan";
        public static final String COLUMN_FOLLOW_UP = "follow_up";
        public static final String COLUMN_DOA = "doa";
        public static final String COLUMN_FOTO = "foto";
        public static final String COLUMN_ID_ANAK = "id_anak";


        // creates a Uri for a specific portofolio
        public static Uri buildPortofolioUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    //nested class defines contents of anak table
    public static final class Anak implements BaseColumns{
        public static final String TABLE_NAME = "anaks";

        //uri for anaks table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        //column name for anak table's column
        public static final String COLUMN_NAMA_ANAK = "nama_anak";
        public static final String COLUMN_NAMA_PANGGILAN_ANAK = "nama_panggilan_anak";
        public static final String COLUMN_JENIS_KELAMIN = "jenis_kelamin";
        public static final String COLUMN_TEMPAT_LAHIR = "tempat_lahir";
        public static final String COLUMN_TANGGAL_LAHIR = "tanggal_lahir";

        //creates a Uri for a specific anak
        public static Uri buildAnakUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }



    }
}
