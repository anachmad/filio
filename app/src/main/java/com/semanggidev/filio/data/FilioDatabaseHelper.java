package com.semanggidev.filio.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.semanggidev.filio.data.DatabaseDescription.Portofolio;
import com.semanggidev.filio.data.DatabaseDescription.Anak;

/**
 * Created by anachmad on 5/21/17.
 */

class FilioDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Filio.db";
    private static final int DATABASE_VERSION = 1;

    // constructor
    public FilioDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // creates the portofolios table when the database is created
    @Override
    public void onCreate(SQLiteDatabase db) {
        addPortofolioTable(db);
        addAnakTable(db);
    }

    private void addPortofolioTable(SQLiteDatabase db){
        // SQL for creating the porfotolio table
        final String CREATE_PORTOFOLIO_TABLE =
                "CREATE TABLE " + Portofolio.TABLE_NAME + "(" +
                        Portofolio._ID + " INTEGER PRIMARY KEY, " +
                        Portofolio.COLUMN_JUDUL_KEGIATAN + " TEXT UNIQUE NOT NULL, " +
                        Portofolio.COLUMN_TEMPAT_KEGIATAN + " TEXT, " +
                        Portofolio.COLUMN_TANGGAL_KEGIATAN + " INTEGER, " +
                        Portofolio.COLUMN_WAKTU_KEGIATAN + " TEXT, " +
                        Portofolio.COLUMN_ID_ANAK + " INTEGER, " +
                        Portofolio.COLUMN_NAMA_ANAK + " TEXT, " +
                        Portofolio.COLUMN_USIA_ANAK + " TEXT, " +
                        Portofolio.COLUMN_DESKRIPSI_KEGIATAN + " TEXT, " +
                        Portofolio.COLUMN_TUJUAN_KEGIATAN + " TEXT, " +
                        Portofolio.COLUMN_FITRAH + " TEXT, " +
                        Portofolio.COLUMN_ASPEK_PERKEMBANGAN + " TEXT, " +
                        Portofolio.COLUMN_FASILITATOR + " TEXT, " +
                        Portofolio.COLUMN_HIKMAH_KEGIATAN + " TEXT, " +
                        Portofolio.COLUMN_FOLLOW_UP + " TEXT, " +
                        Portofolio.COLUMN_DOA + " TEXT, " +
                        Portofolio.COLUMN_EKSPRESI_ANAK + " TEXT, " +
                        Portofolio.COLUMN_URAIAN_EKSPRESI_ANAK + " TEXT, " +
                        Portofolio.COLUMN_FOTO + " TEXT);";
        db.execSQL(CREATE_PORTOFOLIO_TABLE); // create the portofolio table
    }

    private void addAnakTable(SQLiteDatabase db){
        final String CREATE_ANAK_TABLE =
                "CREATE TABLE " + Anak.TABLE_NAME + "(" +
                    Anak._ID + " INTEGER PRIMARY KEY, " +
                    Anak.COLUMN_NAMA_ANAK + " TEXT, " +
                    Anak.COLUMN_NAMA_PANGGILAN_ANAK + " TEXT, " +
                    Anak.COLUMN_JENIS_KELAMIN + " TEXT, " +
                    Anak.COLUMN_TEMPAT_LAHIR + " TEXT, " +
                    Anak.COLUMN_TANGGAL_LAHIR + " INTEGER)";

        db.execSQL(CREATE_ANAK_TABLE);
    }

    // normally defines how to upgrade the database when the schema changes
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) { }
}
