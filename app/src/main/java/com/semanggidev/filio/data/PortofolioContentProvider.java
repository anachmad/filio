package com.semanggidev.filio.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.ContactsContract;

import com.semanggidev.filio.R;
import com.semanggidev.filio.data.DatabaseDescription.Portofolio;
import com.semanggidev.filio.data.DatabaseDescription.Anak;

/**
 * Created by anachmad on 5/21/17.
 */

public class PortofolioContentProvider extends ContentProvider {
    // used to access the database
    private FilioDatabaseHelper dbHelper;

    // UriMatcher helps ContentProvider determine operation to perform
    private static final UriMatcher uriMatcher =
            new UriMatcher(UriMatcher.NO_MATCH);

    // constants used with UriMatcher to determine operation to perform
    private static final int ONE_PORTOFOLIO = 101; // manipulate one portofolio
    private static final int PORTOFOLIOS = 102; // manipulate portofolios table
    private static final int ONE_ANAK = 111; // manipulate one anak
    private static final int ANAKS = 112; // manipulate anaks table


    // static block to configure this ContentProvider's UriMatcher
    static {
        // Uri for portofolio with the specified id (#)
        uriMatcher.addURI(DatabaseDescription.AUTHORITY,
                Portofolio.TABLE_NAME + "/#", ONE_PORTOFOLIO);

        // Uri for portofolios table
        uriMatcher.addURI(DatabaseDescription.AUTHORITY,
                Portofolio.TABLE_NAME, PORTOFOLIOS);

        // Uri for anak with the specified id (#)
        uriMatcher.addURI(DatabaseDescription.AUTHORITY,
                Anak.TABLE_NAME + "/#", ONE_ANAK);

        // Uri for anaks table
        uriMatcher.addURI(DatabaseDescription.AUTHORITY,
                Anak.TABLE_NAME, ANAKS);
    }

    // called when the PortofolioContentProvider is created
    @Override
    public boolean onCreate() {
        // create the AddressBookDatabaseHelper
        dbHelper = new FilioDatabaseHelper(getContext());
        return true; // ContentProvider successfully created
    }

    // required method: Not used in this app, so we return null
    @Override
    public String getType(Uri uri) {
        return null;
    }

    // query the database
    @Override
    public Cursor query(Uri uri, String[] projection,
                        String selection, String[] selectionArgs, String sortOrder) {

        // create SQLiteQueryBuilder for querying portofolios table
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)) {
            case ONE_PORTOFOLIO: // contact with specified id will be selected
                queryBuilder.setTables(Portofolio.TABLE_NAME);
                queryBuilder.appendWhere(
                        Portofolio._ID + "=" + uri.getLastPathSegment());
                break;
            case PORTOFOLIOS: // all contacts will be selected
                queryBuilder.setTables(Portofolio.TABLE_NAME);
                break;
            case ONE_ANAK: // contact with specified id will be selected
                queryBuilder.setTables(Anak.TABLE_NAME);
                queryBuilder.appendWhere(
                        Portofolio._ID + "=" + uri.getLastPathSegment());
                break;
            case ANAKS: // all contacts will be selected
                queryBuilder.setTables(Anak.TABLE_NAME);
                break;

            default:
                throw new UnsupportedOperationException(
                        getContext().getString(R.string.invalid_query_uri) + uri);
        }

        // execute the query to select one or all contacts
        Cursor cursor = queryBuilder.query(dbHelper.getReadableDatabase(),
                projection, selection, selectionArgs, null, null, sortOrder);

        // configure to watch for content changes
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    // insert a new portofolio in the database
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri newPortofolioUri = null;

        switch (uriMatcher.match(uri)) {
            case PORTOFOLIOS:
                // insert the new portofolio--success yields new portofolio's row id
                long rowId = dbHelper.getWritableDatabase().insert(
                        Portofolio.TABLE_NAME, null, values);

                // if the portofolio was inserted, create an appropriate Uri;
                // otherwise, throw an exception
                if (rowId > 0) { // SQLite row IDs start at 1
                    newPortofolioUri = Portofolio.buildPortofolioUri(rowId);

                    // notify observers that the database changed
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                else
                    throw new SQLException(
                            getContext().getString(R.string.insert_failed) + uri);
                break;
            default:
                throw new UnsupportedOperationException(
                        getContext().getString(R.string.invalid_insert_uri) + uri);
        }

        return newPortofolioUri;
    }

    // update an existing portofolio in the database
    @Override
    public int update(Uri uri, ContentValues values,
                      String selection, String[] selectionArgs) {
        int numberOfRowsUpdated; // 1 if update successful; 0 otherwise

        switch (uriMatcher.match(uri)) {
            case ONE_PORTOFOLIO:
                // get from the uri the id of contact to update
                String id = uri.getLastPathSegment();

                // update the contact
                numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                        Portofolio.TABLE_NAME, values, Portofolio._ID + "=" + id,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException(
                        getContext().getString(R.string.invalid_update_uri) + uri);
        }

        // if changes were made, notify observers that the database changed
        if (numberOfRowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numberOfRowsUpdated;
    }

    // delete an existing portofolio from the database
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int numberOfRowsDeleted;

        switch (uriMatcher.match(uri)) {
            case ONE_PORTOFOLIO:
                // get from the uri the id of portofolio to update
                String id = uri.getLastPathSegment();

                // delete the contact
                numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                        Portofolio.TABLE_NAME, Portofolio._ID + "=" + id, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException(
                        getContext().getString(R.string.invalid_delete_uri) + uri);
        }

        // notify observers that the database changed
        if (numberOfRowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numberOfRowsDeleted;
    }
}
