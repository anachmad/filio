package com.semanggidev.filio.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by anachmad on 1/13/18.
 */

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    DatePickerDialogFragmentListener listener;

    public interface DatePickerDialogFragmentListener{
        public void onDatePickerDialogDateSet(int year, int month, int dayOfMonth);
    }

    public static DatePickerDialogFragment newInstance(){
        DatePickerDialogFragment fragment = new DatePickerDialogFragment();
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar =  Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

//        DatePickerDialog.OnDateSetListener listener = (DatePickerDialog.OnDateSetListener) getActivity();

        return new DatePickerDialog(getActivity(),this,year,month,day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        listener = (DatePickerDialogFragmentListener) getTargetFragment();
        listener.onDatePickerDialogDateSet(year, month, dayOfMonth);
        this.dismiss();
    }
}
