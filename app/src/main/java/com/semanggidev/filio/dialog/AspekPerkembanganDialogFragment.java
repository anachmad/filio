package com.semanggidev.filio.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.semanggidev.filio.R;

import java.util.ArrayList;

/**
 * Created by anachmad on 6/7/17.
 */

public class AspekPerkembanganDialogFragment extends DialogFragment {

    public interface AspekPerkembanganDialogListener {
        public void onAspekPerkembanganDialogPositiveClick(String selectedItemsString);
    }

    AspekPerkembanganDialogListener listener;
    String[] listItems;
    boolean[] checkedItem;
    ArrayList<Integer> userItems = new ArrayList<>();

    public AspekPerkembanganDialogFragment(){

    }

    public static AspekPerkembanganDialogFragment newInstance(String selectedItems){
        AspekPerkembanganDialogFragment fragment = new AspekPerkembanganDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("selectedItems",selectedItems);
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        listItems = getResources().getStringArray(R.array.pilihan_aspek_perkembangan);
        checkedItem = new boolean[listItems.length];

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih Aspek Perkembangan");
        builder.setMultiChoiceItems(listItems,checkedItem,new DialogInterface.OnMultiChoiceClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked){
                    if(!userItems.contains(which)){
                        userItems.add(which);
                        checkedItem[which] = true;
                    } else{
                        userItems.remove(userItems.indexOf(which));
                        checkedItem[which] = false;
                    }
                }else{
                    if(userItems.contains(which)){
                        userItems.remove(userItems.indexOf(which));
                        checkedItem[which] = false;
                    }
                }
            }
        });

        builder.setPositiveButton("OK",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selectedItemsString = "";
                for(int i = 0; i < userItems.size(); i++){
                    selectedItemsString = selectedItemsString + listItems[userItems.get(i)];
                    if(i != userItems.size() - 1){
                        selectedItemsString = selectedItemsString + ", ";
                    }
                }
                listener = (AspekPerkembanganDialogListener) getTargetFragment();
                listener.onAspekPerkembanganDialogPositiveClick(selectedItemsString);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });

        return builder.create();
    }

//    private boolean[] getCheckedItems(){
//        listItems = getResources().getStringArray(R.array.pilihan_fitrah);
//        boolean[] checkedItem = new boolean[listItems.length];
//        String selectedFitrah = getArguments().getString("selectedFitrah");
//        String[] selectedFitrahArray = selectedFitrah.split(", ");
//
//        for(int i = 0; i < listItems.length; i++){
//            for(int j = 0; j < selectedFitrahArray.length; j++){
//                if(listItems[i].equals(selectedFitrahArray[j].trim())){
//                    checkedItem[i] = true;
//                }
//                System.out.println(listItems[i]);
//                System.out.println(selectedFitrahArray[j].trim());
//                System.out.println(checkedItem[i]);
//            }
//        }
//        System.out.println(checkedItem.toString());
//        return checkedItem;
//    }
}
