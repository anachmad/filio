package com.semanggidev.filio.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.semanggidev.filio.R;

/**
 * Created by anachmad on 6/10/17.
 */

public class DeletePortofolioDialogFragment extends DialogFragment {

    public interface DeletePortofolioDialogFragmentListener {
        public void onDialogPositiveClick();
    }

    // Use this instance of the interface to deliver action events
    DeletePortofolioDialogFragmentListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle bundle) {
        // create a new AlertDialog Builder
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.confirm_title);
        builder.setMessage(R.string.confirm_message);

        // provide an OK button that simply dismisses the dialog
        builder.setPositiveButton(R.string.button_delete,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int button) {
                    mListener = (DeletePortofolioDialogFragmentListener) getTargetFragment();
                    mListener.onDialogPositiveClick();
                    dismiss();
                }
            });

        builder.setNegativeButton(R.string.button_cancel,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int button) {

                    dismiss();
                }
            });


        return builder.create(); // return the AlertDialog
    }
}
