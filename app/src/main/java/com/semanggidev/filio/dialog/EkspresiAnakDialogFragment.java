package com.semanggidev.filio.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.semanggidev.filio.R;

/**
 * Created by anachmad on 7/24/17.
 */

public class EkspresiAnakDialogFragment
        extends DialogFragment
        implements View.OnClickListener {

    ImageView imageViewSenang;
    ImageView imageViewSedih;
    ImageView imageViewTakut;
    ImageView imageViewMarah;
    ImageView imageViewJijik;
    EkspresiAnakDialogFragmentListener listener;


    public interface EkspresiAnakDialogFragmentListener {
        public void onImageSelected(int id);
    }

    public EkspresiAnakDialogFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_ekspresi_anak,container);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listener = (EkspresiAnakDialogFragmentListener) getTargetFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    private void initView(View view){
        imageViewSenang = (ImageView) view.findViewById(R.id.imageViewSenang);
        imageViewSedih = (ImageView) view.findViewById(R.id.imageViewSedih);
        imageViewMarah = (ImageView) view.findViewById(R.id.imageViewMarah);
        imageViewTakut = (ImageView) view.findViewById(R.id.imageViewTakut);
        imageViewJijik = (ImageView) view.findViewById(R.id.imageViewJijik);

        imageViewSenang.setOnClickListener(this);
        imageViewSedih.setOnClickListener(this);
        imageViewMarah.setOnClickListener(this);
        imageViewTakut.setOnClickListener(this);
        imageViewJijik.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onImageSelected(v.getId());
        getDialog().dismiss();
    }

}
