package com.semanggidev.filio.tool;

import com.esafirm.imagepicker.helper.ImagePickerUtils;
import com.esafirm.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by anachmad on 2/17/18.
 */

public class ImageHandler {
    public static String createImagesPath(List<Image> images) {

        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0, l = images.size(); i < l; i++) {
            stringBuffer.append(images.get(i).getPath()).append(";");
        }
        return stringBuffer.toString();
    }

    public static ArrayList<Image> createImagesFromPath (String path){
        ArrayList<Image> images = new ArrayList<>();
        List<String> imagesPath;

        imagesPath = Arrays.asList(path.split(";"));

        for(int i = 0, j = imagesPath.size(); i < j; i++){
            images.add(new Image(0, ImagePickerUtils.getNameFromFilePath(imagesPath.get(i)), imagesPath.get(i)));
        }

        return images;
    }

    public static ArrayList<String> createImagesPathList(List<Image> images) {

        ArrayList<String> imagesPathList = new ArrayList<>();
        for(int i = 0, j = images.size(); i < j; i++){
            imagesPathList.add(images.get(i).getPath());
        }
        return imagesPathList;

    }
}
