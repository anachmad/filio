package com.semanggidev.filio.tool;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by anachmad on 8/13/17.
 */

public class DateHandler {

    private static final SimpleDateFormat tanggalFormat = new SimpleDateFormat("dd");
    private static final SimpleDateFormat bulanFormat = new SimpleDateFormat("MMMM");
    private static final SimpleDateFormat tahunFormat = new SimpleDateFormat("yyyy");
    private static final SimpleDateFormat hariFormat = new SimpleDateFormat("EEEE");
    private static final SimpleDateFormat waktuFormat = new SimpleDateFormat("H:mm");
    private static final SimpleDateFormat tanggalBulanTahunFormat = new SimpleDateFormat("dd MMMM yyyy");

    public static Date getCurrentDate(){
        return Calendar.getInstance().getTime();
    }

    public static long toUnixTime(Date date){
        return date.getTime()/1000;
    }

    public static long getCurrentUnixTime(){
        return toUnixTime(getCurrentDate());
    }


    public static String getTanggal(long unixTime){
        return tanggalFormat.format(parseDateNew(unixTime));
    }

    public static String getBulan(long unixTime){
        return bulanFormat.format(parseDateNew(unixTime));
    }

    public static String getTahun(long unixTime){
        return tahunFormat.format(parseDateNew(unixTime));
    }

    public static String getWaktu(long unixTime){
        return waktuFormat.format(parseDateNew(unixTime));
    }

    public static String getHari(long unixTime){
        return hariFormat.format(parseDateNew(unixTime));
    }

    public static String getTanggalBulanTahun(long unixTime){
        return tanggalBulanTahunFormat.format(parseDateNew(unixTime));
    }

    public static Date parseDateNew(long unixTime){
        return new Date(unixTime*1000);
    }
}
