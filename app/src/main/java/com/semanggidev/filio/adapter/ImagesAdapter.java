package com.semanggidev.filio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.semanggidev.filio.R;

import java.util.ArrayList;

/**
 * Created by anachmad on 2/17/18.
 */

public class ImagesAdapter extends
        RecyclerView.Adapter<ImagesAdapter.ViewHolder>{

    private ArrayList<String> imagesPathList;
    private Context context;
    private ImageClickListener listener;

    public interface ImageClickListener {
        void onClick(ArrayList<String> imagesPathList, int position);
    }

    public ImagesAdapter(Context context, ArrayList<String> imagesPathList, ImageClickListener listener){
        this.imagesPathList = imagesPathList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.image_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImagesAdapter.ViewHolder holder, int position) {
        String imagePath = imagesPathList.get(position);

        Glide.with(context)
                .load(imagePath)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imagesPathList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_row);

        }
    }
}
