package com.semanggidev.filio.adapter;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.semanggidev.filio.R;
import com.semanggidev.filio.data.DatabaseDescription.Portofolio;
import com.semanggidev.filio.tool.DateHandler;

/**
 * Created by anachmad on 5/22/17.
 */

public class PortofoliosAdapter extends RecyclerView.Adapter<PortofoliosAdapter.ViewHolder>{

    // PortofoliosAdapter instance variables
    private Cursor cursor = null;
    private final PortofolioClickListener clickListener;

    //constructor
    public PortofoliosAdapter(PortofolioClickListener clickListener) {
        this.clickListener = clickListener;
    }

    // interface implemented by PortofoliosFragment to respond
    // when the user touches an item in the RecyclerView
    public interface PortofolioClickListener {
        void onClick(Uri portofolioUri);
    }

    // nested subclass of RecyclerView.ViewHolder used to implement
    // the view-holder pattern in the context of a RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView judulKegiatanRowTextView;
        private final TextView tanggalKegiatanRowTextView;
        private final TextView namaAnakTextView;
        private long rowID;

        // configures a RecyclerView item's ViewHolder
        public ViewHolder(View itemView) {
            super(itemView);
            judulKegiatanRowTextView = (TextView) itemView.findViewById(R.id.judulKegiatanRowTextView);
            tanggalKegiatanRowTextView = (TextView) itemView.findViewById(R.id.tanggalKegiatanRowTextView);
            namaAnakTextView = itemView.findViewById(R.id.namaAnakRowTextView);

            // attach listener to itemView
            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        // executes when the contact in this ViewHolder is clicked
                        @Override
                        public void onClick(View view) {
                            clickListener.onClick(Portofolio.buildPortofolioUri(rowID));
                        }
                    }
            );
        }

        // set the database row ID for the contact in this ViewHolder
        public void setRowID(long rowID) {
            this.rowID = rowID;
        }
    }

    // sets up new list item and its ViewHolder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the android.R.layout.simple_list_item_1 layout
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.portofolio_row, parent, false);
        return new ViewHolder(view); // return current item's ViewHolder
    }

    // sets the text of the list item to display the search tag
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        cursor.moveToPosition(position);
        holder.setRowID(cursor.getLong(cursor.getColumnIndex(Portofolio._ID)));
        holder.judulKegiatanRowTextView.setText(cursor.getString(cursor.getColumnIndex(
                Portofolio.COLUMN_JUDUL_KEGIATAN)));
        holder.tanggalKegiatanRowTextView.setText(DateHandler.getTanggalBulanTahun(cursor.getLong(cursor.getColumnIndex(
                Portofolio.COLUMN_TANGGAL_KEGIATAN))));
        holder.namaAnakTextView.setText(cursor.getString(cursor.getColumnIndex(Portofolio.COLUMN_NAMA_ANAK)));
    }

    // returns the number of items that adapter binds
    @Override
    public int getItemCount() {
        return (cursor != null) ? cursor.getCount() : 0;
    }

    // swap this adapter's current Cursor for a new one
    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }

}
