package com.semanggidev.filio.activity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.semanggidev.filio.R;
import com.semanggidev.filio.fragment.AnakAddEditFragment;
import com.semanggidev.filio.fragment.AnakDetailFragment;
import com.semanggidev.filio.fragment.AnaksFragment;
import com.semanggidev.filio.fragment.PortofolioAddEditFragment;
import com.semanggidev.filio.fragment.PortofolioDetailFragment;
import com.semanggidev.filio.fragment.PortofolioImageFragment;
import com.semanggidev.filio.fragment.PortofoliosFragment;
import com.semanggidev.filio.fragment.TentangFragment;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
    implements PortofoliosFragment.PortofoliosFragmentListener,
        PortofolioDetailFragment.PortofolioDetailFragmentListener,
        PortofolioAddEditFragment.PortofolioAddEditFragmentListener,
        AnaksFragment.AnaksFragmentListener,
        AnakDetailFragment.AnakDetailFragmentListener,
        AnakAddEditFragment.AnakAddEditFragmentListener
    {

    // key for storing a portofolio's Uri in a Bundle passed to a fragment
    public static final String PORTOFOLIO_URI = "portofolio_uri";
    public static final String ANAK_URI = "anak_uri";
    private Toolbar toolbar;
    private ActionBar actionBar;
    private NavigationView navigationView;

    private PortofoliosFragment portofoliosFragment; // displays portofolio list

    // display portofoliosFragment when MainActivity first loads
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initDrawerMenu();

        // if layout contains fragmentContainer, the phone layout is in use;
        // create and display a PortofoliosFragment
        if (savedInstanceState == null &&
                findViewById(R.id.fragmentContainer) != null) {
            // create PortofoliosFragment
            portofoliosFragment = new PortofoliosFragment();

            // add the fragment to the FrameLayout
            FragmentTransaction transaction =
                    getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragmentContainer, portofoliosFragment);
            transaction.commit(); // display PortofoliosFragment
        }
        else {
            portofoliosFragment =
                    (PortofoliosFragment) getSupportFragmentManager().
                            findFragmentById(R.id.portofoliosFragment);
        }

    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);
    }

        private void initDrawerMenu() {
            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                public void onDrawerOpened(View drawerView) {
                    hideKeyboard();
                    super.onDrawerOpened(drawerView);
                }
            };
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    item.setChecked(true);
                    displayFragment(item.getItemId(), item.getTitle().toString());
                    drawer.closeDrawer(Gravity.LEFT);
                    return true;
                }
            });
        }

        private void hideKeyboard() {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPortofolioSelected(Uri portofolioUri) {
        if (findViewById(R.id.fragmentContainer) != null) // phone
            displayPortofolio(portofolioUri, R.id.fragmentContainer);
        else { // tablet
            // removes top of back stack
            getSupportFragmentManager().popBackStack();

            displayPortofolio(portofolioUri, R.id.rightPaneContainer);
        }
    }

    @Override
    public void onAddPortofolio(){
        if (findViewById(R.id.fragmentContainer) != null) // phone
            displayAddEditFragment(R.id.fragmentContainer, null);
        else // tablet
            displayAddEditFragment(R.id.rightPaneContainer, null);
    }

    // display a portofolio
    private void displayPortofolio(Uri portofolioUri, int viewID) {
        PortofolioDetailFragment portofolioDetailFragment = new PortofolioDetailFragment();

        // specify portofolio's Uri as an argument to the PortofolioDetailFragment
        Bundle arguments = new Bundle();
        arguments.putParcelable(PORTOFOLIO_URI, portofolioUri);
        portofolioDetailFragment.setArguments(arguments);

        // use a FragmentTransaction to display the PortofolioDetailFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, portofolioDetailFragment);
        transaction.addToBackStack(null);
        transaction.commit(); // causes PortofolioDetailFragment to display
    }

        // display fragment for adding a new or editing an existing portofolio
    private void displayAddEditFragment(int viewID, Uri portofolioUri) {
        PortofolioAddEditFragment portofolioAddEditFragment = new PortofolioAddEditFragment();

        // if editing existing portofolio, provide portofolioUri as an argument
        if (portofolioUri != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(PORTOFOLIO_URI, portofolioUri);
            portofolioAddEditFragment.setArguments(arguments);
        }

        // use a FragmentTransaction to display the PortofolioAddEditFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, portofolioAddEditFragment);
        transaction.addToBackStack(null);
        transaction.commit(); // causes PortofolioAddEditFragment to display
    }

    // return to portofolio list when displayed portofolio deleted
    @Override
    public void onPortofolioDeleted() {
        // removes top of back stack
        getSupportFragmentManager().popBackStack();
        portofoliosFragment.updatePortofolioList(); // refresh portofolios
    }

    // display the PortofolioAddEditFragment to edit an existing portofolio
    @Override
    public void onEditPortofolio(Uri portofolioUri) {
        if (findViewById(R.id.fragmentContainer) != null) // phone
            displayAddEditFragment(R.id.fragmentContainer, portofolioUri);
        else // tablet
            displayAddEditFragment(R.id.rightPaneContainer, portofolioUri);
    }

    // display the PortofolioImageFragment to show the portofolio image gallery
    @Override
    public void onClickImage(ArrayList<String> imagesPathList) {
        Bundle arguments = new Bundle();
        arguments.putStringArrayList("images_path_list", imagesPathList);

        PortofolioImageFragment fragment = new PortofolioImageFragment();
        fragment.setArguments(arguments);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(getFragmentContainer(),fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

        // update GUI after new portofolio or updated portofolio saved
    @Override
    public void onAddEditCompleted(Uri portofolioUri) {
        // removes top of back stack
        getSupportFragmentManager().popBackStack();
        portofoliosFragment.updatePortofolioList(); // refresh portofolios

        if (findViewById(R.id.fragmentContainer) == null) { // tablet
            // removes top of back stack
            getSupportFragmentManager().popBackStack();

            // on tablet, display portofolio that was just added or edited
            displayPortofolio(portofolioUri, R.id.rightPaneContainer);
        }
    }

    public void displayFragment(int viewId, String title){

        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);

        Fragment fragment = null;
        switch(viewId){
            case R.id.nav_portofolio :
                fragment = new PortofoliosFragment();
                break;
            case R.id.nav_anak :
//                fragment = new AnaksFragment();
                break;
            case R.id.nav_tentang :
                fragment = new TentangFragment();
        }

        if (fragment != null) {
            FragmentTransaction transaction =
                    getSupportFragmentManager().beginTransaction();
            transaction.replace(getFragmentContainer(), fragment);
            transaction.commit();
        }

    }

    public void displayAnak(Uri uri){

    }

    @Override
    public void onAnakSelected(Uri uri) {

    }

    @Override
    public void onAddAnak() {
        displayAnakAddEditFragment(getFragmentContainer(), null);
    }

    public void displayAnakAddEditFragment(int viewId, Uri uri){
        Fragment fragment = new AnakAddEditFragment();

        // if editing existing anak, provide AnakUri as an argument
        if (uri != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(ANAK_URI, uri);
            fragment.setArguments(arguments);
        }

        // use a FragmentTransaction to display the AnakAddEditFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private boolean isPhone(){
        if (findViewById(R.id.fragmentContainer) != null){
            return true;
        } else return false;
    }

    private int getFragmentContainer(){
        if (isPhone()){
            return R.id.fragmentContainer;
        } else return R.id.rightPaneContainer;
    }

        @Override
        public void onAnakDeleted() {

        }

        @Override
        public void onEditAnak(Uri anakUri) {

        }
    }
